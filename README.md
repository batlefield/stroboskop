# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://batlefield@bitbucket.org/batlefield/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/batlefield/stroboskop/commits/8ca2310880c61dbf43ac70ba784d1d88fe0340a7

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/batlefield/stroboskop/commits/64a2df079b774290f69d2a67225a9a87bdb19a43

Naloga 6.3.2:
https://bitbucket.org/batlefield/stroboskop/commits/62b3ce700a85a8d73e5a6297e4fde465e4c32b0b

Naloga 6.3.3:
https://bitbucket.org/batlefield/stroboskop/commits/62198408ec9c3a56da89378cf1f0039d2b2ffbc1

Naloga 6.3.4:
https://bitbucket.org/batlefield/stroboskop/commits/5ffd3c9ae52a4ca83bc83f41c91a56cf559aac97

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/batlefield/stroboskop/commits/f5868927984114b5c90583f40ce55f497f741c2c)

Naloga 6.4.2:
https://bitbucket.org/batlefield/stroboskop/commits/7d94d99f8516e2b715d6b3d6ddceabf66e67f616

Naloga 6.4.3:
https://bitbucket.org/batlefield/stroboskop/commits/9607cc5d0a02fbff1b09f878a117e57c059926da

Naloga 6.4.4:
https://bitbucket.org/batlefield/stroboskop/commits/eef305e6d8fb663a9406ddc1f26d768033143c43